﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	public string axisH;
	public string axisV;
	public float acceleration = 1.0f; // metres per second
	private float speed = 0.0f; // this represent the starting speed of the player
	public float maxSpeed = 5.0f; // maximum possible speed
	public float brake = 5.0f; // in metres/second/s
	public float turnspeed = 30.0f; // in metres / second
	void Update() {
		// get the vertical axis controls acceleration fwd/bck


		float forwards = Input.GetAxis (axisV);
		// the horizontal axis controller

		float turn = Input.GetAxis(axisH);

		// then turn the car
		if (forwards > 0) {
		transform.Rotate(0,0,-turn*(turnspeed-(speed*3) )*Time.deltaTime);

			


			//accelerate forwards

			speed = speed + acceleration * Time.deltaTime;
		}
		else if ( forwards < 0 ){
			transform.Rotate(0,0,-turn*(turnspeed-(-speed*3) )*Time.deltaTime);
			//Accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			if (speed < 0.5f && speed > -0.5f)
				speed = 0;
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else if (speed < 0) {
				speed = speed + brake * Time.deltaTime;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}

}
